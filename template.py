# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: AGPL-3.0-or-later
import flask
import glob
import os

# same dir
import config
import blog

template_args = None


def latest_news_parse():
    """ :returns: (latest_news_link, latest_news_title) """
    pattern = f"{config.srcdir}/content/blog/*.md"
    post = os.path.basename(sorted(glob.glob(pattern), reverse=True)[0])
    post_parsed = blog.parse_post(post, create_html=False)

    return post_parsed["url"], post_parsed["title-short"]


def init_global_template_args():
    """ Initialize the global template_args variable once. """
    global template_args

    latest_news_link, latest_news_title = latest_news_parse()

    template_args = {"latest_news_link": latest_news_link,
                     "latest_news_title": latest_news_title}


def render_template(*args, **kwargs):
    """ Wrapper for flask.render_template, where we can pre-define passed
        arguments, so they will be available for every template (e.g. latest
        news blog post.) """
    global template_args

    if not template_args:
        init_global_template_args()

    return flask.render_template(*args, **kwargs, **template_args)
