title: "Merch"
preview: "merch/phone_in_shirt.jpg"
---

[![](/static/img/merch/phone_in_shirt_thumb.jpg){: class="wfull border" }](/static/img/merch/phone_in_shirt.jpg)

If you are looking for a sustainable, stylish piece of clothing with a
postmarketOS logo to either keep your phone, yourself or even your cat comfy,
you came to the right place. Below you will find the official postmarketOS
merch sites. When buying a piece of merch from either of these, you are at the
same time making a small donation towards postmarketOS.

[#grid side#]
[![](/static/img/merch/originals.jpg){: class="w300 border" }](https://postmarketos.teemill.com/)
[#grid text#]

## Original T-Shirts Designs

From the logo in 3D to the super minimalist variant that features the logo
without text, here are the Tees that you might have seen people wear at FOSDEM
and other cool events. If this is too casual for your rebel soul and you would
rather wear a fist on your chest with some text that makes the reader
seriously question their choice of operating system, we got that as well now.
And if that sounds too scary and you prefer a cute cat chewing on an N900...
you guessed it, it's in there as well.

=> **[postmarketos.teemill.com](https://postmarketos.teemill.com)**

[#grid end#]
[#grid side#]
[![](/static/img/merch/hellotux.jpg){: class="w300 border" }](https://www.hellotux.com/postmarketos)
[#grid text#]

## HELLOTUX

If you would rather have a really nice embroidered version of the logo or
don't want to limit yourself to shirts, then HELLOTUX is the place to go. This
is a little family owned shop from people who are truly into the spirit of free
software and running our favorite kernel, to the point that they run their
whole operation on Linux (more about that on their
[about](https://www.hellotux.com/aboutus.php) page).

=> **[hellotux.com](https://www.hellotux.com/postmarketos)**

[#grid end#]

[#grid side#]
[![](/static/img/merch/pinephone.jpg){: class="w300 border" }](https://pine64eu.com)
[#grid text#]

## Devices with postmarketOS Pre-Installed

If you are reading these lines, you probably know what the
[PinePhone](https://wiki.postmarketos.org/wiki/PINE64_PinePhone_(pine64-pinephone))
and
[PinePhone Pro](https://wiki.postmarketos.org/wiki/PINE64_PinePhone_Pro_(pine64-pinephonepro))
are. When buying one from the PINE64 EU store, you will
get the stable version of postmarketOS flashed right to the eMMC. Ready
for you to power it on and go through the on-device installer to set up
your encrypted installation. Just make sure you have the right
expectations: this is for developers. As of writing especially the
PinePhone Pro has a long way to go so you probably shouldn't get one
unless you intend to contribute to development.

=> **[pine64eu.com](https://pine64eu.com)**
[#grid end#]
